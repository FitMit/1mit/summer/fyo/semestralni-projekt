#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <list>

#include <QMainWindow>
#include <QGraphicsScene>

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

    void hst_draw_rays();
    void hst_draw_mirrors();
    bool hst_shown = true;
    QGraphicsScene *hst_scene;
    std::list<QGraphicsLineItem*> hst_rays_list;
    QGraphicsPathItem *hst_primary_mirror_path = NULL;
    QGraphicsPathItem *hst_secondary_mirror_path = NULL;
    int hst_rays_angle = 0;
    QColor hst_rays_color = Qt::red;
    QColor hst_mirror_color = Qt::black;
    int hst_mirrors_scale = 100;
    float hst_mirrors_separation = 4.6 * hst_mirrors_scale;
    int hst_primary_mirror_x = 800;
    int hst_primary_mirror_y = 300;
    int hst_primary_mirror_angle = 0;
    float hst_primary_mirror_diameter = 2.4;
    float hst_primary_mirror_roc = 11.04;
    int hst_secondary_mirror_x = hst_primary_mirror_x-hst_mirrors_separation;
    int hst_secondary_mirror_y = 300;
    int hst_secondary_mirror_angle = 0;
    float hst_secondary_mirror_diameter = 0.31;
    float hst_secondary_mirror_roc = 1.36;

    void jwst_draw_rays();
    void jwst_draw_mirrors();
    bool jwst_shown = false;
    QGraphicsScene *jwst_scene;
    std::list<QGraphicsLineItem*> jwst_rays_list;
    QGraphicsPathItem *jwst_primary_mirror_path = NULL;
    QGraphicsPathItem *jwst_secondary_mirror_path = NULL;
    QGraphicsPathItem *jwst_tertiary_mirror_path = NULL;
    QGraphicsPathItem *jwst_fourth_mirror_path = NULL;
    int jwst_rays_angle = 0;
    QColor jwst_rays_color = Qt::red;
    QColor jwst_mirror_color = Qt::black;
    int jwst_mirrors_scale = 100;
    int jwst_primary_mirror_x = 800;
    int jwst_primary_mirror_y = 250;
    int jwst_primary_mirror_angle = 0;
    float jwst_primary_mirror_diameter = 6.6052;
    float jwst_primary_mirror_roc = 15.8797;
    float jwst_primary_secondary_mirror_separation = 7.169 * jwst_mirrors_scale;
    int jwst_secondary_mirror_x = jwst_primary_mirror_x-jwst_primary_secondary_mirror_separation;
    int jwst_secondary_mirror_y = 250;
    int jwst_secondary_mirror_angle = 0;
    float jwst_secondary_mirror_diameter = 0.738;
    float jwst_secondary_mirror_roc = 1.7789;
    float jwst_primary_tertiary_mirror_separation_x = -0.7963*jwst_mirrors_scale;
    float jwst_primary_tertiary_mirror_separation_y = -0.00019*jwst_mirrors_scale;
    int jwst_tertiary_mirror_x = jwst_primary_mirror_x-jwst_primary_tertiary_mirror_separation_x;
    int jwst_tertiary_mirror_y = jwst_primary_mirror_y-jwst_primary_tertiary_mirror_separation_y;;
    int jwst_tertiary_mirror_angle = 0;
    float jwst_tertiary_mirror_diameter = 0.517;
    float jwst_tertiary_mirror_roc = 3.0162;
    float jwst_primary_fourth_mirror_separation_x = 1.0478*jwst_mirrors_scale;
    float jwst_primary_fourth_mirror_separation_y = -0.00236*jwst_mirrors_scale;
    int jwst_fourth_mirror_x = jwst_primary_mirror_x-jwst_primary_fourth_mirror_separation_x;
    int jwst_fourth_mirror_y = jwst_primary_mirror_y-jwst_primary_fourth_mirror_separation_y;;
    int jwst_fourth_mirror_angle = 0;
    float jwst_fourth_mirror_lenght = 0.1725;


public slots:
    void show_HST_visualization();
    void hst_rays_show(int state);
    void hst_rays_color_select();
    void hst_mirror_color_select();
    void hst_rays_angle_changed(int value);
    void hst_reset_scene();
    void hst_primary_mirror_x_changed(int value);
    void hst_primary_mirror_y_changed(int value);
    void hst_primary_mirror_diameter_changed(double value);
    void hst_primary_mirror_roc_changed(double value);
    void hst_primary_mirror_angle_changed(int value);
    void hst_secondary_mirror_x_changed(int value);
    void hst_secondary_mirror_y_changed(int value);
    void hst_secondary_mirror_diameter_changed(double value);
    void hst_secondary_mirror_roc_changed(double value);
    void hst_secondary_mirror_angle_changed(int value);

    void show_JWST_visualization();
    void jwst_rays_show(int state);
    void jwst_rays_color_select();
    void jwst_mirror_color_select();
    void jwst_rays_angle_changed(int value);
    void jwst_reset_scene();
    void jwst_primary_mirror_x_changed(int value);
    void jwst_primary_mirror_y_changed(int value);
    void jwst_primary_mirror_diameter_changed(double value);
    void jwst_primary_mirror_roc_changed(double value);
    void jwst_primary_mirror_angle_changed(int value);
    void jwst_secondary_mirror_x_changed(int value);
    void jwst_secondary_mirror_y_changed(int value);
    void jwst_secondary_mirror_diameter_changed(double value);
    void jwst_secondary_mirror_roc_changed(double value);
    void jwst_secondary_mirror_angle_changed(int value);
    void jwst_tertiary_mirror_x_changed(int value);
    void jwst_tertiary_mirror_y_changed(int value);
    void jwst_tertiary_mirror_diameter_changed(double value);
    void jwst_tertiary_mirror_roc_changed(double value);
    void jwst_tertiary_mirror_angle_changed(int value);
    void jwst_fourth_mirror_x_changed(int value);
    void jwst_fourth_mirror_y_changed(int value);
    void jwst_fourth_mirror_lenght_changed(double value);
    void jwst_fourth_mirror_angle_changed(int value);

private:
    Ui::MainWindow *ui;
};
#endif // MAINWINDOW_H
