#include "mainwindow.h"
#include "./ui_mainwindow.h"

#include <cmath>
#include <math.h>

#include <QColorDialog>
#include <QLayout>
#include <QGraphicsLineItem>
#include <QDebug>

////////////////////////////////////////////// Window functions //////////////////////////////////////////////
MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    // Show HST
    this->setWindowTitle("Vizualizace HST");
    ui->jwst_frame->hide();

    // Add menu actions to select HST or JWST visualisation
    QAction *hst_action = menuBar()->addAction("HST");
    QAction *jwst_action = menuBar()->addAction("JWST");

    // Connect scene selection
    connect(hst_action, SIGNAL(triggered()), this, SLOT(show_HST_visualization()));
    connect(jwst_action, SIGNAL(triggered()), this, SLOT(show_JWST_visualization()));

    // Antialising in graphics windows
    ui->hst_graphics->setRenderHint(QPainter::Antialiasing);
    ui->jwst_graphics->setRenderHint(QPainter::Antialiasing);

    // Start scenes
    hst_scene = new QGraphicsScene(this);
    hst_scene->setSceneRect(0, 0, ui->hst_graphics->width(), ui->hst_frame->height() - ui->hst_control_widget->height() - ui->menubar->height());
    ui->hst_graphics->setScene(hst_scene);

    jwst_scene = new QGraphicsScene(this);
    jwst_scene->setSceneRect(0, 0, ui->jwst_graphics->width(), ui->jwst_frame->height() - ui->jwst_control_widget->height() - ui->menubar->height());
    ui->jwst_graphics->setScene(jwst_scene);

    //////////////////////// Setup visualizations HST ////////////////////////
    // Connects
    connect(ui->hst_rays_togle, SIGNAL(stateChanged(int)), this, SLOT(hst_rays_show(int)));
    connect(ui->hst_ray_color_button, SIGNAL(clicked()), this, SLOT(hst_rays_color_select()));
    connect(ui->hst_mirror_color_button, SIGNAL(clicked()), this, SLOT(hst_mirror_color_select()));
    connect(ui->hst_rays_angle_spin, SIGNAL(valueChanged(int)), this, SLOT(hst_rays_angle_changed(int)));
    connect(ui->hst_reset_button, SIGNAL(clicked()), this, SLOT(hst_reset_scene()));

    connect(ui->hst_primary_mirror_x_spin, SIGNAL(valueChanged(int)), this, SLOT(hst_primary_mirror_x_changed(int)));
    connect(ui->hst_primary_mirror_y_spin, SIGNAL(valueChanged(int)), this, SLOT(hst_primary_mirror_y_changed(int)));
    connect(ui->hst_primary_mirror_diameter_spin, SIGNAL(valueChanged(double)), this, SLOT(hst_primary_mirror_diameter_changed(double)));
    connect(ui->hst_primary_mirror_roc_spin, SIGNAL(valueChanged(double)), this, SLOT(hst_primary_mirror_roc_changed(double)));
    connect(ui->hst_primary_mirror_angle_spin, SIGNAL(valueChanged(int)), this, SLOT(hst_primary_mirror_angle_changed(int)));

    connect(ui->hst_secondary_mirror_x_spin, SIGNAL(valueChanged(int)), this, SLOT(hst_secondary_mirror_x_changed(int)));
    connect(ui->hst_secondary_mirror_y_spin, SIGNAL(valueChanged(int)), this, SLOT(hst_secondary_mirror_y_changed(int)));
    connect(ui->hst_secondary_mirror_diameter_spin, SIGNAL(valueChanged(double)), this, SLOT(hst_secondary_mirror_diameter_changed(double)));
    connect(ui->hst_secondary_mirror_roc_spin, SIGNAL(valueChanged(double)), this, SLOT(hst_secondary_mirror_roc_changed(double)));
    connect(ui->hst_secondary_mirror_angle_spin, SIGNAL(valueChanged(int)), this, SLOT(hst_secondary_mirror_angle_changed(int)));

    // Values
    ui->hst_rays_angle_spin->setValue(hst_rays_angle);

    ui->hst_primary_mirror_x_spin->setValue(hst_primary_mirror_x);
    ui->hst_primary_mirror_y_spin->setValue(hst_primary_mirror_y);
    ui->hst_primary_mirror_diameter_spin->setValue(hst_primary_mirror_diameter);
    ui->hst_primary_mirror_roc_spin->setValue(hst_primary_mirror_roc);
    ui->hst_primary_mirror_angle_spin->setValue(hst_primary_mirror_angle);

    ui->hst_secondary_mirror_x_spin->setValue(hst_secondary_mirror_x);
    ui->hst_secondary_mirror_y_spin->setValue(hst_secondary_mirror_y);
    ui->hst_secondary_mirror_diameter_spin->setValue(hst_secondary_mirror_diameter);
    ui->hst_secondary_mirror_roc_spin->setValue(hst_secondary_mirror_roc);
    ui->hst_secondary_mirror_angle_spin->setValue(hst_secondary_mirror_angle);

    hst_draw_mirrors();

    //////////////////////// Setup visualizations JWST ////////////////////////
    // Connects
    connect(ui->jwst_rays_togle, SIGNAL(stateChanged(int)), this, SLOT(jwst_rays_show(int)));
    connect(ui->jwst_ray_color_button, SIGNAL(clicked()), this, SLOT(jwst_rays_color_select()));
    connect(ui->jwst_mirror_color_button, SIGNAL(clicked()), this, SLOT(jwst_mirror_color_select()));
    connect(ui->jwst_rays_angle_spin, SIGNAL(valueChanged(int)), this, SLOT(jwst_rays_angle_changed(int)));
    connect(ui->jwst_reset_button, SIGNAL(clicked()), this, SLOT(jwst_reset_scene()));

    connect(ui->jwst_primary_mirror_x_spin, SIGNAL(valueChanged(int)), this, SLOT(jwst_primary_mirror_x_changed(int)));
    connect(ui->jwst_primary_mirror_y_spin, SIGNAL(valueChanged(int)), this, SLOT(jwst_primary_mirror_y_changed(int)));
    connect(ui->jwst_primary_mirror_diameter_spin, SIGNAL(valueChanged(double)), this, SLOT(jwst_primary_mirror_diameter_changed(double)));
    connect(ui->jwst_primary_mirror_roc_spin, SIGNAL(valueChanged(double)), this, SLOT(jwst_primary_mirror_roc_changed(double)));
    connect(ui->jwst_primary_mirror_angle_spin, SIGNAL(valueChanged(int)), this, SLOT(jwst_primary_mirror_angle_changed(int)));

    connect(ui->jwst_secondary_mirror_x_spin, SIGNAL(valueChanged(int)), this, SLOT(jwst_secondary_mirror_x_changed(int)));
    connect(ui->jwst_secondary_mirror_y_spin, SIGNAL(valueChanged(int)), this, SLOT(jwst_secondary_mirror_y_changed(int)));
    connect(ui->jwst_secondary_mirror_diameter_spin, SIGNAL(valueChanged(double)), this, SLOT(jwst_secondary_mirror_diameter_changed(double)));
    connect(ui->jwst_secondary_mirror_roc_spin, SIGNAL(valueChanged(double)), this, SLOT(jwst_secondary_mirror_roc_changed(double)));
    connect(ui->jwst_secondary_mirror_angle_spin, SIGNAL(valueChanged(int)), this, SLOT(jwst_secondary_mirror_angle_changed(int)));

    connect(ui->jwst_tertiary_mirror_x_spin, SIGNAL(valueChanged(int)), this, SLOT(jwst_tertiary_mirror_x_changed(int)));
    connect(ui->jwst_tertiary_mirror_y_spin, SIGNAL(valueChanged(int)), this, SLOT(jwst_tertiary_mirror_y_changed(int)));
    connect(ui->jwst_tertiary_mirror_diameter_spin, SIGNAL(valueChanged(double)), this, SLOT(jwst_tertiary_mirror_diameter_changed(double)));
    connect(ui->jwst_tertiary_mirror_roc_spin, SIGNAL(valueChanged(double)), this, SLOT(jwst_tertiary_mirror_roc_changed(double)));
    connect(ui->jwst_tertiary_mirror_angle_spin, SIGNAL(valueChanged(int)), this, SLOT(jwst_tertiary_mirror_angle_changed(int)));

    connect(ui->jwst_fourth_mirror_x_spin, SIGNAL(valueChanged(int)), this, SLOT(jwst_fourth_mirror_x_changed(int)));
    connect(ui->jwst_fourth_mirror_y_spin, SIGNAL(valueChanged(int)), this, SLOT(jwst_fourth_mirror_y_changed(int)));
    connect(ui->jwst_fourth_mirror_lenght_spin, SIGNAL(valueChanged(double)), this, SLOT(jwst_fourth_mirror_lenght_changed(double)));
    connect(ui->jwst_fourth_mirror_angle_spin, SIGNAL(valueChanged(int)), this, SLOT(jwst_fourth_mirror_angle_changed(int)));

    // Values
    ui->jwst_rays_angle_spin->setValue(jwst_rays_angle);

    ui->jwst_primary_mirror_x_spin->setValue(jwst_primary_mirror_x);
    ui->jwst_primary_mirror_y_spin->setValue(jwst_primary_mirror_y);
    ui->jwst_primary_mirror_diameter_spin->setValue(jwst_primary_mirror_diameter);
    ui->jwst_primary_mirror_roc_spin->setValue(jwst_primary_mirror_roc);
    ui->jwst_primary_mirror_angle_spin->setValue(jwst_primary_mirror_angle);

    ui->jwst_secondary_mirror_x_spin->setValue(jwst_secondary_mirror_x);
    ui->jwst_secondary_mirror_y_spin->setValue(jwst_secondary_mirror_y);
    ui->jwst_secondary_mirror_diameter_spin->setValue(jwst_secondary_mirror_diameter);
    ui->jwst_secondary_mirror_roc_spin->setValue(jwst_secondary_mirror_roc);
    ui->jwst_secondary_mirror_angle_spin->setValue(jwst_secondary_mirror_angle);

    ui->jwst_tertiary_mirror_x_spin->setValue(jwst_tertiary_mirror_x);
    ui->jwst_tertiary_mirror_y_spin->setValue(jwst_tertiary_mirror_y);
    ui->jwst_tertiary_mirror_diameter_spin->setValue(jwst_tertiary_mirror_diameter);
    ui->jwst_tertiary_mirror_roc_spin->setValue(jwst_tertiary_mirror_roc);
    ui->jwst_tertiary_mirror_angle_spin->setValue(jwst_tertiary_mirror_angle);

    ui->jwst_fourth_mirror_x_spin->setValue(jwst_fourth_mirror_x);
    ui->jwst_fourth_mirror_y_spin->setValue(jwst_fourth_mirror_y);
    ui->jwst_fourth_mirror_lenght_spin->setValue(jwst_fourth_mirror_lenght);
    ui->jwst_fourth_mirror_angle_spin->setValue(jwst_fourth_mirror_angle);

    //jwst_draw_mirrors();
}

MainWindow::~MainWindow()
{
    ///////////////// Delete HST ////////////////
    // Delete hst rays
    for(QGraphicsLineItem* x : hst_rays_list)
    {
        hst_scene->removeItem(x);
        delete x;
    }

    // Delete hst mirrors
    if (hst_primary_mirror_path)
    {
        hst_scene->removeItem(hst_primary_mirror_path);
        delete hst_primary_mirror_path;
    }

    if (hst_secondary_mirror_path)
    {
        hst_scene->removeItem(hst_secondary_mirror_path);
        delete hst_secondary_mirror_path;
    }

    ///////////////// Delete JWST ////////////////
    // Delete jwst rays
    for(QGraphicsLineItem* x : jwst_rays_list)
    {
        jwst_scene->removeItem(x);
        delete x;
    }

    // Delete jwst mirrors
    if (jwst_primary_mirror_path)
    {
        jwst_scene->removeItem(jwst_primary_mirror_path);
        delete jwst_primary_mirror_path;
    }

    if (jwst_secondary_mirror_path)
    {
        jwst_scene->removeItem(jwst_secondary_mirror_path);
        delete jwst_secondary_mirror_path;
    }

    if (jwst_tertiary_mirror_path)
    {
        jwst_scene->removeItem(jwst_tertiary_mirror_path);
        delete jwst_tertiary_mirror_path;
    }

    if (jwst_fourth_mirror_path)
    {
        jwst_scene->removeItem(jwst_fourth_mirror_path);
        delete jwst_fourth_mirror_path;
    }

    delete ui;
    delete hst_scene;
    delete jwst_scene;
}

// Change to HST window
void MainWindow::show_HST_visualization()
{
    // Switch visualization
    if (!hst_shown)
    {
        this->setWindowTitle("Vizualizace HST");

        ui->hst_frame->show();
        ui->jwst_frame->hide();

        hst_shown = true;
        jwst_shown = false;
    }
}

// Change to JWST window
void MainWindow::show_JWST_visualization()
{
    // Switch visualization
    if (!jwst_shown)
    {
        this->setWindowTitle("Vizualizace JWST");

        ui->hst_frame->hide();
        ui->jwst_frame->show();

        hst_shown = false;
        jwst_shown = true;
    }
}

////////////////////////////////////////////// HST functions //////////////////////////////////////////////
// Show mirrors in HST scene
void MainWindow::hst_draw_mirrors()
{
    // Delete old mirrors
    if (hst_primary_mirror_path)
    {
        hst_scene->removeItem(hst_primary_mirror_path);
        delete hst_primary_mirror_path;
    }

    if (hst_secondary_mirror_path)
    {
        hst_scene->removeItem(hst_secondary_mirror_path);
        delete hst_secondary_mirror_path;
    }


    // Pen for mirrors
    QPen mirror_pen = QPen(hst_mirror_color);
    mirror_pen.setWidth(2);

    ////////////////////// Primary mirror //////////////////////////
    int radius_of_curvature_primary = hst_primary_mirror_roc*hst_mirrors_scale;
    int diameter_primary = hst_primary_mirror_diameter*hst_mirrors_scale;
    float angle_in_degrees_primary = asin((diameter_primary/2.0)/radius_of_curvature_primary)/(M_PI/180.0);

    int radius_of_curvature_secondary = hst_secondary_mirror_roc*hst_mirrors_scale;
    int diameter_secondary = hst_secondary_mirror_diameter*hst_mirrors_scale;
    float angle_in_degrees_secondary = asin((diameter_secondary/2.0)/radius_of_curvature_secondary)/(M_PI/180.0);

    // Gap degrees based on secondary mirror diameter
    float gap_degrees = (asin((diameter_secondary/2.0)/radius_of_curvature_primary)/(M_PI/180.0))*1.2;

    QPainterPath primary_mirror_path = QPainterPath();
    primary_mirror_path.moveTo(hst_primary_mirror_x, hst_primary_mirror_y);

    //path->quadTo(400,200,400,400);
    //path->cubicTo(200+(100*0.05), 200 - (50*4.0/3.0), (200+100)-(100*0.05), 200 - (50*4.0/3.0),300,200);

    //qInfo() << "ROC = " << radius_of_curvature << ", Diameter = " << diameter << ", Angle =" << angle_in_degrees;

    primary_mirror_path.arcMoveTo(hst_primary_mirror_x-radius_of_curvature_primary,hst_primary_mirror_y-(radius_of_curvature_primary/2),radius_of_curvature_primary,radius_of_curvature_primary,gap_degrees);
    primary_mirror_path.arcTo(hst_primary_mirror_x-radius_of_curvature_primary,hst_primary_mirror_y-(radius_of_curvature_primary/2),radius_of_curvature_primary,radius_of_curvature_primary,gap_degrees, angle_in_degrees_primary-gap_degrees);
    primary_mirror_path.arcMoveTo(hst_primary_mirror_x-radius_of_curvature_primary,hst_primary_mirror_y-(radius_of_curvature_primary/2),radius_of_curvature_primary,radius_of_curvature_primary,-gap_degrees);
    primary_mirror_path.arcTo(hst_primary_mirror_x-radius_of_curvature_primary,hst_primary_mirror_y-(radius_of_curvature_primary/2),radius_of_curvature_primary,radius_of_curvature_primary,-gap_degrees, -angle_in_degrees_primary+gap_degrees);

    // Baffle
    int offset = hst_mirrors_separation*1.1;
    primary_mirror_path.arcMoveTo((hst_primary_mirror_x-radius_of_curvature_primary)-offset,hst_primary_mirror_y-(radius_of_curvature_primary/2),radius_of_curvature_primary,radius_of_curvature_primary,0);
    primary_mirror_path.arcTo((hst_primary_mirror_x-radius_of_curvature_primary)-offset,hst_primary_mirror_y-(radius_of_curvature_primary/2),radius_of_curvature_primary,radius_of_curvature_primary,0, gap_degrees);
    primary_mirror_path.arcMoveTo((hst_primary_mirror_x-radius_of_curvature_primary)-offset,hst_primary_mirror_y-(radius_of_curvature_primary/2),radius_of_curvature_primary,radius_of_curvature_primary,0);
    primary_mirror_path.arcTo((hst_primary_mirror_x-radius_of_curvature_primary)-offset,hst_primary_mirror_y-(radius_of_curvature_primary/2),radius_of_curvature_primary,radius_of_curvature_primary,0, -gap_degrees);

    // Rotate
    QTransform t;
    t.rotate(hst_primary_mirror_angle);

    primary_mirror_path.translate(-hst_primary_mirror_x, -hst_primary_mirror_y);
    primary_mirror_path = t.map(primary_mirror_path);
    primary_mirror_path.translate(hst_primary_mirror_x, hst_primary_mirror_y);

    hst_primary_mirror_path = hst_scene->addPath(primary_mirror_path,mirror_pen);

    ////////////////////// Secondary mirror //////////////////////////
    //qInfo() << "ROC = " << radius_of_curvature << ", Diameter = " << diameter << ", Angle =" << angle_in_degrees;

    QPainterPath secondary_mirror_path = QPainterPath();
    secondary_mirror_path.moveTo(hst_secondary_mirror_x,hst_secondary_mirror_y);

    //secondary_mirror_path.arcMoveTo(hst_secondary_mirror_x,hst_secondary_mirror_y,radius_of_curvature,radius_of_curvature,0);
    secondary_mirror_path.arcTo(hst_secondary_mirror_x-radius_of_curvature_secondary,hst_secondary_mirror_y-(radius_of_curvature_secondary/2),radius_of_curvature_secondary,radius_of_curvature_secondary,0, angle_in_degrees_secondary);
    secondary_mirror_path.arcMoveTo(hst_secondary_mirror_x-radius_of_curvature_secondary,hst_secondary_mirror_y-(radius_of_curvature_secondary/2),radius_of_curvature_secondary,radius_of_curvature_secondary,0);
    secondary_mirror_path.arcTo(hst_secondary_mirror_x-radius_of_curvature_secondary,hst_secondary_mirror_y-(radius_of_curvature_secondary/2),radius_of_curvature_secondary,radius_of_curvature_secondary,0, -angle_in_degrees_secondary);

    // Rotate
    t.reset();
    t.rotate(hst_secondary_mirror_angle);

    secondary_mirror_path.translate(-hst_secondary_mirror_x, -hst_secondary_mirror_y);
    secondary_mirror_path = t.map(secondary_mirror_path);
    secondary_mirror_path.translate(hst_secondary_mirror_x, hst_secondary_mirror_y);

    hst_secondary_mirror_path = hst_scene->addPath(secondary_mirror_path,mirror_pen);
}

// Show rays in HST scene
void MainWindow::hst_draw_rays()
{
    //////////////////////////// Setup /////////////////////////
    // Check draw rays indicator
    if (!(ui->hst_rays_togle->isChecked()))
    {
        return;
    }

    // Remove old rays
    for(QGraphicsLineItem* x : hst_rays_list)
    {
        hst_scene->removeItem(x);
        delete x;
    }

    // Delete list items
    hst_rays_list.clear();

    ////////////////////////// Rays /////////////////////////
    // Print rays with reflections
    int hst_scene_height = ui->hst_frame->height() - ui->hst_control_widget->height() - ui->menubar->height();
    int rays_separation = 30;

    for (int y = rays_separation; y < hst_scene_height; y+=rays_separation)
    {
        // Line parameters
        int x1 = 0;
        int y1 = y;
        int x2;
        int y2;

        // covert degrees to radians
        float angle_radians = hst_rays_angle / (180/M_PI);

        // Calculate end point from angle
        float y2_diference = tan(angle_radians) * (ui->hst_graphics->width());
        int y2_from_angle = y + y2_diference;

        y2 = y2_from_angle;
        x2 = ui->hst_graphics->width();

        /*
        // Rectify y2 if out of bounds for scene height
        if (y2 < 0)
        {
            // Angle from x1=0,y2=0,x2=0,y=max line
            float beta = 90 + hst_rays_angle;
            float beta_radians = beta / (180/M_PI);

            int distance_from_first_row = y1;
            float x2_diference = tan(beta_radians) * (distance_from_first_row);

            if (x2 > (x1 + x2_diference))
            {
                x2 = x1 + x2_diference;
            }
            y2 = 3;
        }
        else if(y2 > hst_scene_height)
        {
            // Angle from x1=0,y2=max,x2=max,y=max line
            float beta = 90 - hst_rays_angle;
            float beta_radians = beta / (180/M_PI);

            int distance_from_last_row = hst_scene_height - y1;
            float x2_diference = tan(beta_radians) * (distance_from_last_row);

            if (x2 > (x1 + x2_diference))
            {
                x2 = x1 + x2_diference;
            }
            y2 = hst_scene_height - 3;
        }
        */

        // TODO check intersect

        QPen ray_pen =  QPen(hst_rays_color);
        QGraphicsLineItem *ray = hst_scene->addLine(x1, y1, x2, y2, ray_pen);
        hst_rays_list.push_back(ray);

        //qInfo() << "Y = " << y << ", Rays angle = " << hst_rays_angle << ", Tan =" << tan(angle_radians);
    }
}

// HST select rays color
void MainWindow::hst_rays_color_select()
{
    QColor tmp_color = QColorDialog::getColor(hst_rays_color, this);
    if (tmp_color.isValid())
    {
        hst_rays_color = tmp_color;
    }

    hst_draw_rays();
}

// HST select mirror color
void MainWindow::hst_mirror_color_select()
{
    QColor tmp_color = QColorDialog::getColor(hst_mirror_color, this);
    if (tmp_color.isValid())
    {
        hst_mirror_color = tmp_color;
    }

    hst_draw_mirrors();
    hst_draw_rays();
}

// HST start/stop rays
void MainWindow::hst_rays_show(int state)
{
    if (state)
    {
        // Show rays
        hst_draw_rays();
    }
    else
    {
        // Hide rays
        for(QGraphicsLineItem* x : hst_rays_list)
        {
            hst_scene->removeItem(x);
            delete x;
        }

        hst_rays_list.clear();
    }
}

// HST get rays angle
void MainWindow::hst_rays_angle_changed(int value)
{
    hst_rays_angle = ui->hst_rays_angle_spin->value();
    hst_draw_rays();
}

// HST get primary mirror x position
void MainWindow::hst_primary_mirror_x_changed(int value)
{
    hst_primary_mirror_x = ui->hst_primary_mirror_x_spin->value();
    hst_draw_mirrors();
    hst_draw_rays();
}

// HST get primary mirror y position
void MainWindow::hst_primary_mirror_y_changed(int value)
{
    hst_primary_mirror_y = ui->hst_primary_mirror_y_spin->value();
    hst_draw_mirrors();
    hst_draw_rays();
}

// HST get primary mirror diameter
void MainWindow::hst_primary_mirror_diameter_changed(double value)
{
    hst_primary_mirror_diameter = ui->hst_primary_mirror_diameter_spin->value();
    hst_draw_mirrors();
    hst_draw_rays();
}

// HST get primary mirror radius of curviture
void MainWindow::hst_primary_mirror_roc_changed(double value)
{
    hst_primary_mirror_roc = ui->hst_primary_mirror_roc_spin->value();
    hst_draw_mirrors();
    hst_draw_rays();
}

// HST get primary mirror radius of curviture
void MainWindow::hst_primary_mirror_angle_changed(int value)
{
    hst_primary_mirror_angle = ui->hst_primary_mirror_angle_spin->value();
    hst_draw_mirrors();
    hst_draw_rays();
}

// HST get secondary mirror x position
void MainWindow::hst_secondary_mirror_x_changed(int value)
{
    hst_secondary_mirror_x = ui->hst_secondary_mirror_x_spin->value();
    hst_draw_mirrors();
    hst_draw_rays();
}

// HST get secondary mirror y position
void MainWindow::hst_secondary_mirror_y_changed(int value)
{
    hst_secondary_mirror_y = ui->hst_secondary_mirror_y_spin->value();
    hst_draw_mirrors();
    hst_draw_rays();
}

// HST get secondary mirror diameter
void MainWindow::hst_secondary_mirror_diameter_changed(double value)
{
    hst_secondary_mirror_diameter = ui->hst_secondary_mirror_diameter_spin->value();
    hst_draw_mirrors();
    hst_draw_rays();
}

// HST get secondary mirror radius of curvature
void MainWindow::hst_secondary_mirror_roc_changed(double value)
{
    hst_secondary_mirror_roc = ui->hst_secondary_mirror_roc_spin->value();
    hst_draw_mirrors();
    hst_draw_rays();
}

// HST get secondary mirror angle
void MainWindow::hst_secondary_mirror_angle_changed(int value)
{
    hst_secondary_mirror_angle = ui->hst_secondary_mirror_angle_spin->value();
    hst_draw_mirrors();
    hst_draw_rays();
}

// HST reset scene parameters
void MainWindow::hst_reset_scene()
{
    // Clear rays
    for(QGraphicsLineItem* x : hst_rays_list)
    {
        hst_scene->removeItem(x);
        delete x;
    }

    hst_rays_list.clear();

    // Reset ray parameters
    hst_rays_angle = 0;
    ui->hst_rays_angle_spin->setValue(hst_rays_angle);
    hst_rays_color = Qt::red;
    hst_mirror_color = Qt::black;

    // Clear mirrors
    if (hst_primary_mirror_path)
    {
        hst_scene->removeItem(hst_primary_mirror_path);
        //delete hst_primary_mirror_path;
    }

    if (hst_secondary_mirror_path)
    {
        hst_scene->removeItem(hst_secondary_mirror_path);
        //delete hst_secondary_mirror_path;
    }

    // Reset mirror parameters
    // TODO values
    hst_primary_mirror_x = 800;
    hst_primary_mirror_y = 300;
    hst_primary_mirror_diameter = 2.4;
    hst_primary_mirror_roc = 11.04;
    hst_primary_mirror_angle = 0;

    ui->hst_primary_mirror_x_spin->setValue(hst_primary_mirror_x);
    ui->hst_primary_mirror_y_spin->setValue(hst_primary_mirror_y);
    ui->hst_primary_mirror_diameter_spin->setValue(hst_primary_mirror_diameter);
    ui->hst_primary_mirror_roc_spin->setValue(hst_primary_mirror_roc);
    ui->hst_primary_mirror_angle_spin->setValue(hst_primary_mirror_angle);

    hst_secondary_mirror_x = hst_primary_mirror_x-hst_mirrors_separation;
    hst_secondary_mirror_y = 300;
    hst_secondary_mirror_diameter = 0.31;
    hst_secondary_mirror_roc = 1.36;
    hst_secondary_mirror_angle = 0;

    ui->hst_secondary_mirror_x_spin->setValue(hst_secondary_mirror_x);
    ui->hst_secondary_mirror_y_spin->setValue(hst_secondary_mirror_y);
    ui->hst_secondary_mirror_diameter_spin->setValue(hst_secondary_mirror_diameter);
    ui->hst_secondary_mirror_roc_spin->setValue(hst_secondary_mirror_roc);
    ui->hst_secondary_mirror_angle_spin->setValue(hst_secondary_mirror_angle);

    hst_draw_mirrors();
    hst_draw_rays();
}

////////////////////////////////////////////// JWST functions //////////////////////////////////////////////
// Show mirrors in HST scene
void MainWindow::jwst_draw_mirrors()
{
    // Clear jwst mirrors
    if (jwst_primary_mirror_path)
    {
        jwst_scene->removeItem(jwst_primary_mirror_path);
        delete jwst_primary_mirror_path;
    }

    if (jwst_secondary_mirror_path)
    {
        jwst_scene->removeItem(jwst_secondary_mirror_path);
        delete jwst_secondary_mirror_path;
    }

    if (jwst_tertiary_mirror_path)
    {
        jwst_scene->removeItem(jwst_tertiary_mirror_path);
        delete jwst_tertiary_mirror_path;
    }

    if (jwst_fourth_mirror_path)
    {
        jwst_scene->removeItem(jwst_fourth_mirror_path);
        delete jwst_fourth_mirror_path;
    }

    // Pen for mirrors
    QPen mirror_pen =  QPen(jwst_mirror_color);
    mirror_pen.setWidth(2);

    ////////////////////// Primary mirror //////////////////////////
    int radius_of_curvature_primary = jwst_primary_mirror_roc*jwst_mirrors_scale;
    int diameter_primary = jwst_primary_mirror_diameter*jwst_mirrors_scale;
    float angle_in_degrees_primary = asin((diameter_primary/2.0)/radius_of_curvature_primary)/(M_PI/180.0);

    int radius_of_curvature_secondary = jwst_secondary_mirror_roc*jwst_mirrors_scale;
    int diameter_secondary = jwst_secondary_mirror_diameter*jwst_mirrors_scale;
    float angle_in_degrees_secondary = asin((diameter_secondary/2.0)/radius_of_curvature_secondary)/(M_PI/180.0);

    int radius_of_curvature_tertiary = jwst_tertiary_mirror_roc*jwst_mirrors_scale;
    int diameter_tertiary = jwst_tertiary_mirror_diameter*jwst_mirrors_scale;
    float angle_in_degrees_tertiary = asin((diameter_tertiary/1.0)/radius_of_curvature_tertiary)/(M_PI/180.0);

    // Gap degrees based on secondary mirror diameter
    float gap_degrees = (asin((diameter_secondary/2.0)/radius_of_curvature_primary)/(M_PI/180.0))*1.2;

    QPainterPath primary_mirror_path = QPainterPath();
    primary_mirror_path.moveTo(jwst_primary_mirror_x, jwst_primary_mirror_y);

    //path->quadTo(400,200,400,400);
    //path->cubicTo(200+(100*0.05), 200 - (50*4.0/3.0), (200+100)-(100*0.05), 200 - (50*4.0/3.0),300,200);

    //qInfo() << "ROC_ter = " << radius_of_curvature_tertiary << ", Diameter_ter = " << diameter_tertiary << ", Angle =" << angle_in_degrees_tertiary;

    primary_mirror_path.arcMoveTo(jwst_primary_mirror_x-radius_of_curvature_primary,jwst_primary_mirror_y-(radius_of_curvature_primary/2),radius_of_curvature_primary,radius_of_curvature_primary,gap_degrees);
    primary_mirror_path.arcTo(jwst_primary_mirror_x-radius_of_curvature_primary,jwst_primary_mirror_y-(radius_of_curvature_primary/2),radius_of_curvature_primary,radius_of_curvature_primary,gap_degrees, angle_in_degrees_primary-gap_degrees);
    primary_mirror_path.arcMoveTo(jwst_primary_mirror_x-radius_of_curvature_primary,jwst_primary_mirror_y-(radius_of_curvature_primary/2),radius_of_curvature_primary,radius_of_curvature_primary,-gap_degrees);
    primary_mirror_path.arcTo(jwst_primary_mirror_x-radius_of_curvature_primary,jwst_primary_mirror_y-(radius_of_curvature_primary/2),radius_of_curvature_primary,radius_of_curvature_primary,-gap_degrees, -angle_in_degrees_primary+gap_degrees);

    // Baffle
    int offset = jwst_primary_secondary_mirror_separation*1.1;
    primary_mirror_path.arcMoveTo((jwst_primary_mirror_x-radius_of_curvature_primary)-offset,jwst_primary_mirror_y-(radius_of_curvature_primary/2),radius_of_curvature_primary,radius_of_curvature_primary,0);
    primary_mirror_path.arcTo((jwst_primary_mirror_x-radius_of_curvature_primary)-offset,jwst_primary_mirror_y-(radius_of_curvature_primary/2),radius_of_curvature_primary,radius_of_curvature_primary,0, gap_degrees);
    primary_mirror_path.arcMoveTo((jwst_primary_mirror_x-radius_of_curvature_primary)-offset,jwst_primary_mirror_y-(radius_of_curvature_primary/2),radius_of_curvature_primary,radius_of_curvature_primary,0);
    primary_mirror_path.arcTo((jwst_primary_mirror_x-radius_of_curvature_primary)-offset,jwst_primary_mirror_y-(radius_of_curvature_primary/2),radius_of_curvature_primary,radius_of_curvature_primary,0, -gap_degrees);

    // Rotate
    QTransform t;
    t.rotate(jwst_primary_mirror_angle);

    primary_mirror_path.translate(-jwst_primary_mirror_x, -jwst_primary_mirror_y);
    primary_mirror_path = t.map(primary_mirror_path);
    primary_mirror_path.translate(jwst_primary_mirror_x, jwst_primary_mirror_y);

    jwst_primary_mirror_path = jwst_scene->addPath(primary_mirror_path,mirror_pen);

    ////////////////////// Secondary mirror //////////////////////////
    //qInfo() << "ROC = " << radius_of_curvature << ", Diameter = " << diameter << ", Angle =" << angle_in_degrees;

    QPainterPath secondary_mirror_path = QPainterPath();
    secondary_mirror_path.moveTo(jwst_secondary_mirror_x,jwst_secondary_mirror_y);

    //secondary_mirror_path.arcMoveTo(hst_secondary_mirror_x,hst_secondary_mirror_y,radius_of_curvature,radius_of_curvature,0);
    secondary_mirror_path.arcTo(jwst_secondary_mirror_x-radius_of_curvature_secondary,jwst_secondary_mirror_y-(radius_of_curvature_secondary/2),radius_of_curvature_secondary,radius_of_curvature_secondary,0, angle_in_degrees_secondary);
    secondary_mirror_path.arcMoveTo(jwst_secondary_mirror_x-radius_of_curvature_secondary,jwst_secondary_mirror_y-(radius_of_curvature_secondary/2),radius_of_curvature_secondary,radius_of_curvature_secondary,0);
    secondary_mirror_path.arcTo(jwst_secondary_mirror_x-radius_of_curvature_secondary,jwst_secondary_mirror_y-(radius_of_curvature_secondary/2),radius_of_curvature_secondary,radius_of_curvature_secondary,0, -angle_in_degrees_secondary);

    // Rotate
    t.reset();
    t.rotate(jwst_secondary_mirror_angle);

    secondary_mirror_path.translate(-jwst_secondary_mirror_x, -jwst_secondary_mirror_y);
    secondary_mirror_path = t.map(secondary_mirror_path);
    secondary_mirror_path.translate(jwst_secondary_mirror_x, jwst_secondary_mirror_y);

    jwst_secondary_mirror_path = jwst_scene->addPath(secondary_mirror_path,mirror_pen);

    ////////////////////// Tertiary mirror //////////////////////////
    QPainterPath tertiary_mirror_path = QPainterPath();
    tertiary_mirror_path.moveTo(jwst_tertiary_mirror_x,jwst_tertiary_mirror_y);

    // Calculated from half point in one side mirror
    tertiary_mirror_path.arcTo(jwst_tertiary_mirror_x-radius_of_curvature_tertiary,jwst_tertiary_mirror_y-(radius_of_curvature_tertiary/2),radius_of_curvature_tertiary,radius_of_curvature_tertiary,0, angle_in_degrees_tertiary);

    // Rotate
    t.reset();
    t.rotate(jwst_tertiary_mirror_angle);

    tertiary_mirror_path.translate(-jwst_tertiary_mirror_x, -jwst_tertiary_mirror_y);
    tertiary_mirror_path = t.map(tertiary_mirror_path);
    tertiary_mirror_path.translate(jwst_tertiary_mirror_x, jwst_tertiary_mirror_y);

    jwst_tertiary_mirror_path = jwst_scene->addPath(tertiary_mirror_path,mirror_pen);

    /// ////////////////////// Fourth mirror //////////////////////////
    QPainterPath fourth_mirror_path = QPainterPath();
    fourth_mirror_path.moveTo(jwst_fourth_mirror_x,jwst_fourth_mirror_y);

    // Only line
    fourth_mirror_path.moveTo(jwst_fourth_mirror_x,jwst_fourth_mirror_y);
    fourth_mirror_path.lineTo(jwst_fourth_mirror_x, jwst_fourth_mirror_y + (jwst_fourth_mirror_lenght*jwst_mirrors_scale));

    // Rotate
    t.reset();
    t.rotate(jwst_fourth_mirror_angle);

    fourth_mirror_path.translate(-jwst_fourth_mirror_x, -jwst_fourth_mirror_y);
    fourth_mirror_path = t.map(fourth_mirror_path);
    fourth_mirror_path.translate(jwst_fourth_mirror_x, jwst_fourth_mirror_y);

    jwst_fourth_mirror_path = jwst_scene->addPath(fourth_mirror_path,mirror_pen);
}


// Show rays in JWST scene
void MainWindow::jwst_draw_rays()
{
    //////////////////////////// Setup /////////////////////////
    // Check draw rays indicator
    if (!(ui->jwst_rays_togle->isChecked()))
    {
        return;
    }

    // Remove old rays
    for(QGraphicsLineItem* x : jwst_rays_list)
    {
        jwst_scene->removeItem(x);
        delete x;
    }

    // Delete list items
    jwst_rays_list.clear();

    ////////////////////////// Rays /////////////////////////
    // Print rays with reflections
    int jwst_scene_height = ui->jwst_frame->height() - ui->jwst_control_widget->height() - ui->menubar->height();
    int first_line_correction = 15;
    int rays_separation = 30;

    for (int y = rays_separation-first_line_correction; y < jwst_scene_height; y+=rays_separation)
    {
        // Line parameters
        int x1 = 0;
        int y1 = y;
        int x2;
        int y2;

        // covert degrees to radians
        float angle_radians = jwst_rays_angle / (180/M_PI);

        // Calculate end point from angle
        float y2_diference = tan(angle_radians) * (ui->jwst_graphics->width());
        int y2_from_angle = y + y2_diference;

        y2 = y2_from_angle;
        x2 = ui->jwst_graphics->width();

        /*
        // Rectify y2 if out of bounds for scene height
        if (y2 < 0)
        {
            // Angle from x1=0,y2=0,x2=0,y=max line
            float beta = 90 + hst_rays_angle;
            float beta_radians = beta / (180/M_PI);

            int distance_from_first_row = y1;
            float x2_diference = tan(beta_radians) * (distance_from_first_row);

            if (x2 > (x1 + x2_diference))
            {
                x2 = x1 + x2_diference;
            }
            y2 = 3;
        }
        else if(y2 > hst_scene_height)
        {
            // Angle from x1=0,y2=max,x2=max,y=max line
            float beta = 90 - hst_rays_angle;
            float beta_radians = beta / (180/M_PI);

            int distance_from_last_row = hst_scene_height - y1;
            float x2_diference = tan(beta_radians) * (distance_from_last_row);

            if (x2 > (x1 + x2_diference))
            {
                x2 = x1 + x2_diference;
            }
            y2 = hst_scene_height - 3;
        }
        */

        // TODO check intersect

        QPen ray_pen =  QPen(jwst_rays_color);
        QGraphicsLineItem *ray = jwst_scene->addLine(x1, y1, x2, y2, ray_pen);
        jwst_rays_list.push_back(ray);

        //qInfo() << "Y = " << y << ", Rays angle = " << hst_rays_angle << ", Tan =" << tan(angle_radians);
    }
}

// JWST select rays color
void MainWindow::jwst_rays_color_select()
{
    QColor tmp_color = QColorDialog::getColor(jwst_rays_color, this);
    if (tmp_color.isValid())
    {
        jwst_rays_color = tmp_color;
    }

    jwst_draw_rays();
}

// JWST select mirror color
void MainWindow::jwst_mirror_color_select()
{
    QColor tmp_color = QColorDialog::getColor(jwst_mirror_color, this);
    if (tmp_color.isValid())
    {
        jwst_mirror_color = tmp_color;
    }

    jwst_draw_mirrors();
    jwst_draw_rays();
}

// JWST start/stop rays
void MainWindow::jwst_rays_show(int state)
{
    if (state)
    {
        // Show rays
        jwst_draw_rays();
    }
    else
    {
        // Hide rays
        for(QGraphicsLineItem* x : jwst_rays_list)
        {
            jwst_scene->removeItem(x);
            delete x;
        }

        jwst_rays_list.clear();
    }
}

// JWST get rays angle
void MainWindow::jwst_rays_angle_changed(int value)
{
    jwst_rays_angle = ui->jwst_rays_angle_spin->value();
    jwst_draw_rays();
}

// JWST reset scene parameters
void MainWindow::jwst_reset_scene()
{
    // Clear rays
    for(QGraphicsLineItem* x : jwst_rays_list)
    {
        jwst_scene->removeItem(x);
        delete x;
    }

    jwst_rays_list.clear();

    // Reset ray parameters
    jwst_rays_angle = 0;
    ui->jwst_rays_angle_spin->setValue(jwst_rays_angle);
    jwst_rays_color = Qt::red;
    jwst_mirror_color = Qt::black;

    // Clear jwst mirrors
    if (jwst_primary_mirror_path)
    {
        jwst_scene->removeItem(jwst_primary_mirror_path);
        //delete jwst_primary_mirror_path;
    }

    if (jwst_secondary_mirror_path)
    {
        jwst_scene->removeItem(jwst_secondary_mirror_path);
        //delete jwst_secondary_mirror_path;
    }

    if (jwst_tertiary_mirror_path)
    {
        jwst_scene->removeItem(jwst_tertiary_mirror_path);
        //delete jwst_tertiary_mirror_path;
    }

    if (jwst_fourth_mirror_path)
    {
        jwst_scene->removeItem(jwst_fourth_mirror_path);
        //delete jwst_fourth_mirror_path;
    }

    // Reset mirror parameters
    // TODO values
    jwst_primary_mirror_x = 800;
    jwst_primary_mirror_y = 250;
    jwst_primary_mirror_angle = 0;
    jwst_primary_mirror_diameter = 6.605;
    jwst_primary_mirror_roc = 15.88;

    ui->jwst_primary_mirror_x_spin->setValue(jwst_primary_mirror_x);
    ui->jwst_primary_mirror_y_spin->setValue(jwst_primary_mirror_y);
    ui->jwst_primary_mirror_diameter_spin->setValue(jwst_primary_mirror_diameter);
    ui->jwst_primary_mirror_roc_spin->setValue(jwst_primary_mirror_roc);
    ui->jwst_primary_mirror_angle_spin->setValue(jwst_primary_mirror_angle);

    jwst_secondary_mirror_x = jwst_primary_mirror_x-jwst_primary_secondary_mirror_separation;
    jwst_secondary_mirror_y = 250;
    jwst_secondary_mirror_angle = 0;
    jwst_secondary_mirror_diameter = 0.738;
    jwst_secondary_mirror_roc = 1.779;

    ui->jwst_secondary_mirror_x_spin->setValue(jwst_secondary_mirror_x);
    ui->jwst_secondary_mirror_y_spin->setValue(jwst_secondary_mirror_y);
    ui->jwst_secondary_mirror_diameter_spin->setValue(jwst_secondary_mirror_diameter);
    ui->jwst_secondary_mirror_roc_spin->setValue(jwst_secondary_mirror_roc);
    ui->jwst_secondary_mirror_angle_spin->setValue(jwst_secondary_mirror_angle);

    jwst_tertiary_mirror_x = jwst_primary_mirror_x-jwst_primary_tertiary_mirror_separation_x;
    jwst_tertiary_mirror_y = jwst_primary_mirror_y-jwst_primary_tertiary_mirror_separation_y;;
    jwst_tertiary_mirror_angle = 0;
    jwst_tertiary_mirror_diameter = 0.728;
    jwst_tertiary_mirror_roc = 3.016;

    ui->jwst_tertiary_mirror_x_spin->setValue(jwst_tertiary_mirror_x);
    ui->jwst_tertiary_mirror_y_spin->setValue(jwst_tertiary_mirror_y);
    ui->jwst_tertiary_mirror_diameter_spin->setValue(jwst_tertiary_mirror_diameter);
    ui->jwst_tertiary_mirror_roc_spin->setValue(jwst_tertiary_mirror_roc);
    ui->jwst_tertiary_mirror_angle_spin->setValue(jwst_tertiary_mirror_angle);

    jwst_fourth_mirror_x = jwst_primary_mirror_x-jwst_primary_fourth_mirror_separation_x;
    jwst_fourth_mirror_y = jwst_primary_mirror_y-jwst_primary_fourth_mirror_separation_y;;
    jwst_fourth_mirror_angle = 0;
    jwst_fourth_mirror_lenght = 0.173;

    ui->jwst_fourth_mirror_x_spin->setValue(jwst_fourth_mirror_x);
    ui->jwst_fourth_mirror_y_spin->setValue(jwst_fourth_mirror_y);
    ui->jwst_fourth_mirror_lenght_spin->setValue(jwst_fourth_mirror_lenght);
    ui->jwst_fourth_mirror_angle_spin->setValue(jwst_fourth_mirror_angle);

    //hst_draw_mirrors();
    jwst_draw_rays();
}

// JWST get primary mirror x position
void MainWindow::jwst_primary_mirror_x_changed(int value)
{
    jwst_primary_mirror_x = ui->jwst_primary_mirror_x_spin->value();
    jwst_draw_mirrors();
    jwst_draw_rays();
}

// JWST get primary mirror y position
void MainWindow::jwst_primary_mirror_y_changed(int value)
{
    jwst_primary_mirror_y = ui->jwst_primary_mirror_y_spin->value();
    jwst_draw_mirrors();
    jwst_draw_rays();
}

// HST get primary mirror diameter
void MainWindow::jwst_primary_mirror_diameter_changed(double value)
{
    jwst_primary_mirror_diameter = ui->jwst_primary_mirror_diameter_spin->value();
    jwst_draw_mirrors();
    jwst_draw_rays();
}

// JWST get primary mirror radius of curviture
void MainWindow::jwst_primary_mirror_roc_changed(double value)
{
    jwst_primary_mirror_roc = ui->jwst_primary_mirror_roc_spin->value();
    jwst_draw_mirrors();
    jwst_draw_rays();
}

// JWST get primary mirror radius of curviture
void MainWindow::jwst_primary_mirror_angle_changed(int value)
{
    jwst_primary_mirror_angle = ui->jwst_primary_mirror_angle_spin->value();
    jwst_draw_mirrors();
    jwst_draw_rays();
}

// JWST get secondary mirror x position
void MainWindow::jwst_secondary_mirror_x_changed(int value)
{
    jwst_secondary_mirror_x = ui->jwst_secondary_mirror_x_spin->value();
    jwst_draw_mirrors();
    jwst_draw_rays();
}

// JWST get secondary mirror y position
void MainWindow::jwst_secondary_mirror_y_changed(int value)
{
    jwst_secondary_mirror_y = ui->jwst_secondary_mirror_y_spin->value();
    jwst_draw_mirrors();
    jwst_draw_rays();
}

// JWST get secondary mirror diameter
void MainWindow::jwst_secondary_mirror_diameter_changed(double value)
{
    jwst_secondary_mirror_diameter = ui->jwst_secondary_mirror_diameter_spin->value();
    jwst_draw_mirrors();
    jwst_draw_rays();
}

// JWST get secondary mirror radius of curvature
void MainWindow::jwst_secondary_mirror_roc_changed(double value)
{
    jwst_secondary_mirror_roc = ui->jwst_secondary_mirror_roc_spin->value();
    jwst_draw_mirrors();
    jwst_draw_rays();
}

// JWST get secondary mirror angle
void MainWindow::jwst_secondary_mirror_angle_changed(int value)
{
    jwst_secondary_mirror_angle = ui->jwst_secondary_mirror_angle_spin->value();
    jwst_draw_mirrors();
    jwst_draw_rays();
}

// JWST get tertiary mirror x position
void MainWindow::jwst_tertiary_mirror_x_changed(int value)
{
    jwst_tertiary_mirror_x = ui->jwst_tertiary_mirror_x_spin->value();
    jwst_draw_mirrors();
    jwst_draw_rays();
}

// JWST get tertiary mirror y position
void MainWindow::jwst_tertiary_mirror_y_changed(int value)
{
    jwst_tertiary_mirror_y = ui->jwst_tertiary_mirror_y_spin->value();
    jwst_draw_mirrors();
    jwst_draw_rays();
}

// JWST get tertiary mirror diameter
void MainWindow::jwst_tertiary_mirror_diameter_changed(double value)
{
    jwst_tertiary_mirror_diameter = ui->jwst_tertiary_mirror_diameter_spin->value();
    jwst_draw_mirrors();
    jwst_draw_rays();
}

// JWST get tertiary mirror radius of curvature
void MainWindow::jwst_tertiary_mirror_roc_changed(double value)
{
    jwst_tertiary_mirror_roc = ui->jwst_tertiary_mirror_roc_spin->value();
    jwst_draw_mirrors();
    jwst_draw_rays();
}

// JWST get tertiary mirror angle
void MainWindow::jwst_tertiary_mirror_angle_changed(int value)
{
    jwst_tertiary_mirror_angle = ui->jwst_tertiary_mirror_angle_spin->value();
    jwst_draw_mirrors();
    jwst_draw_rays();
}

// JWST get fourth mirror x position
void MainWindow::jwst_fourth_mirror_x_changed(int value)
{
    jwst_fourth_mirror_x = ui->jwst_fourth_mirror_x_spin->value();
    jwst_draw_mirrors();
    jwst_draw_rays();
}

// JWST get fourth mirror y position
void MainWindow::jwst_fourth_mirror_y_changed(int value)
{
    jwst_fourth_mirror_y = ui->jwst_fourth_mirror_y_spin->value();
    jwst_draw_mirrors();
    jwst_draw_rays();
}

// JWST get fourth mirror lenght
void MainWindow::jwst_fourth_mirror_lenght_changed(double value)
{
    jwst_fourth_mirror_lenght = ui->jwst_fourth_mirror_lenght_spin->value();
    jwst_draw_mirrors();
    jwst_draw_rays();
}

// JWST get fourth mirror angle
void MainWindow::jwst_fourth_mirror_angle_changed(int value)
{
    jwst_fourth_mirror_angle = ui->jwst_fourth_mirror_angle_spin->value();
    jwst_draw_mirrors();
    jwst_draw_rays();
}
